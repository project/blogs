# Contents of this file

* Introduction
* Recommended modules
* Installation
* Configuration
* Maintainers


# Introduction

Blogs in intended for sites hosting multiple bloggers.

It provides a page and block that lists all bloggers having at least
one blog post on the site, sorted descening (most recent posting
first).

If the user profile has data in a text field with the machine name
`field_blog_title`, this will be used as the blog title instead of the
user name.  Note that the module will not *create* this field for you,
se under “Configuration”  below.


# Requirements

* [Advanced help hint][1]:<br>
  Required to link help text to online help and advanced help.

# Recommended modules

* [Advanced help][2]:<br>
  When this module is enabled, the module's `README.md` may be
  rendered in the browser.
* [Markdown filter][3]:<br>
  When this module is enabled, the module's `README.md` will be
  rendered with the markdown filter.

# Installation

Install as you would normally install a contributed Drupal
module. See: [Installing modules][4] for further information.

# Configuration

To configure the module, navigate to *Administration » Configuration »
People » Blogs*.

If you change the configuration, remember to press *Save
configuration* to make the changes effective.

If you want your bloggers to give their blog a title navigate to
*Administration » Configuration » People » Account settings » Manage
fields*, and add a `Text` field named “Blog title” .  Make sure that
the machine name is “`field_blog_title`”.  If this field exists,
**Blogs** will use this as the blog's title instead of the user name.

To show the list of blogs in a block, navigate to *Administration »
Structure » Blocks* and move the block named “Blogs list” to a region,

To see the list of blogs on a page, visit the path `blogs`.

After changing the configuration, make sure you clear the cache.

# Maintainer

* [gisle](https://www.drupal.org/u/gisle)

Any help with development (patches, reviews, comments) are welcome.


[1]: https://www.drupal.org/project/advanced_help_hint
[2]: https://www.drupal.org/project/advanced_help
[3]: https://www.drupal.org/project/markdown
[4]: https://www.drupal.org/documentation/install/modules-themes/modules-7
